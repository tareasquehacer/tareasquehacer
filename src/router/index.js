import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Board from '@/components/Board'
import Coins from '@/components/Coins'

Vue.use(Router)

let router = new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/Board',
			name: 'Board',
			component: Board,
			meta: {
        requiresAuth: true
      }
		},
		{
			path: '/coins/:id',
			name: 'Coins',
			component: Coins
		}
	]
})

router.beforeEach((to, from, next) => {
	if(to.matched.some(record => record.meta.requiresAuth)) {
		if (localStorage.getItem('auth') == null) {
      next({ name: 'Home'})
      console.log('Not logged');
    } else {
    	console.log(localStorage.getItem('id'))
    	next()
    }
	} else {
		next()
	}
})

export default router